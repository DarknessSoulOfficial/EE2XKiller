import ctypes
import frida
from os import getcwd
import os
import time
def LoadKernel32():
    try:
        return ctypes.cdll.LoadLibrary("Kernel32.dll")
    except(Exception, FileNotFoundError):
        print("Kernel32 not Found")
        exit(443)
def SetConsoleTitle(titlename : str):
    return LoadKernel32().SetConsoleTitleW(titlename)
def LoadEE2XKiller():
    try:
        return ctypes.cdll.LoadLibrary(getcwd() + "\\EE2XKiller.dll")
    except(FileExistsError, FileNotFoundError, Exception):
        print("EE2XKiller Is Not Founded")
        exit(312)
def KillProcessExplorer():
    return LoadEE2XKiller().KillProcessExplorer()
if __name__ == "__main__":
    SetConsoleTitle("EE2XKiller by DarknessSoul")
    KillProcessExplorer()
    while True:
        attach_explorer = frida.attach("explorer.exe")
        if not attach_explorer:
            print("Waiting To Relaunch Explorer...")
            os.system("cls")
