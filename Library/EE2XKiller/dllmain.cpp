// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <Windows.h>
#include <process.h>
#include <Tlhelp32.h>
#include <winbase.h>
#include <string.h>
#include <iostream>
#define PYTHON_DLLEXPORT extern "C" __declspec(dllexport)

PYTHON_DLLEXPORT void KillProcessExplorer() 
{
    HANDLE handle_snap = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
    PROCESSENTRY32 pEntry;
    pEntry.dwSize = sizeof(pEntry);
    BOOL hRes = Process32First(handle_snap, &pEntry);
    while (hRes)
    {
        if (strcmp(pEntry.szExeFile, "explorer.exe") == 0)
        {
            HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
                (DWORD)pEntry.th32ProcessID);
            if (hProcess != NULL)
            {
                TerminateProcess(hProcess, 5500);
                CloseHandle(hProcess);
            }
        }
        hRes = Process32Next(handle_snap, &pEntry);
    }
    std::cout << "Process Explorer is Killed Successfulled!!!" << std::endl;
    CloseHandle(handle_snap);
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        std::cout << "EE2XKiller is Attached Successfully!!!" << std::endl;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

